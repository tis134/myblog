<%-- 
    Document   : edit
    Created on : 16.12.2021, 12:30:17
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div>
    <c:if test="${error ne null}">
         <p class="name notif">${error}</p>
     </c:if>

     <form id="ajax-contact-form" method="post" action="edit">
            <div class="labels">
              <p>
                <label for="title">Название</label>
                <br />
                <input class="required inpt" type="text" name="title" id="name" value="${product.title}" />
              </p>
              <p>
                <label for="text">Описание</label>
                <br />
                <textarea name="text">  ${product.text}
                </textarea>
              </p>
               <input type="hidden" name="id" value="${product.id}">
              <button> Сохранить</button>
            </div>
          </form>
</div>