<%-- 
    Document   : index
    Created on : 28.11.2021, 15:57:23
    Author     : danil
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
  <div id="wrapper">    
    <div class="intro">
      <h1>В аптечном оптовом складе вы можете более выгодно приобретать медицинские товары и получать их с помощью курьера или самовывозом. </h1>
    </div>   
    <div id="about">
        <c:forEach var="product" items="${product}">
      <div class="one-fourth"><a href="services?id=${product.id}"><img src="img/add.jpg" alt="" weight='350' height='150'/></a>
        <h4>${product.title}</h4>
        <p>${product.text}</p>
      </div>
      </c:forEach> 
    </div>  
  </div> 
  <div class="clearfix"></div>
  <div class="push"></div>  
  
