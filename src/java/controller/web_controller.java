/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ejb.EJB;
import java.util.Enumeration;
import java.util.Date;
import entity.Articles;
import session.ArticlesFacade;

/**
 *
 * @author danil
 */
@WebServlet(name = "web_controller",loadOnStartup=1, urlPatterns = {"/services", "/registration","/edit","/add","/delete"})
public class web_controller extends HttpServlet {

@EJB
    ArticlesFacade productFacade;

    @Override
    public void init() throws ServletException {
        getServletContext().setAttribute("product", productFacade.findAll());
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String userPath=request.getServletPath();
        if ("/services".equals(userPath)){
            String id=null;
            Enumeration<String> params = request.getParameterNames();
            while (params.hasMoreElements()) {
                String param = params.nextElement();
                id="id".equals(param)?request.getParameter(param):id;
            }
            try{
                Articles article = productFacade.find(Integer.parseInt(id));
                request.setAttribute("product", article);
            }catch(Exception e){
                e.printStackTrace();
            }
        }else
        if ("/registration".equals(userPath)){
            //TODO: обработка запроса регистрации
        }else
        if ("/edit".equals(userPath)){
            String  
                    title = request.getParameter("title"),
                    text = ((request.getParameter("text") != null && !request.getParameter("text").isEmpty()) ?  request.getParameter("text") : "Нет данных"),
                    id = request.getParameter("id");
            Date date = new Date();
             
            Enumeration<String> params = request.getParameterNames();
            while (params.hasMoreElements()) {
                String param = params.nextElement();
                id="id".equals(param)?request.getParameter(param):id;
            }
            try{
                Articles article = productFacade.find(Integer.parseInt(id));
                request.setAttribute("product", article);
            }catch(Exception e){
                e.printStackTrace();
            }

            String codeOperation = productFacade.updateProduct(Integer.parseInt(id),  title, text , date);
            if (codeOperation != "Успешное обнавление товара") request.setAttribute("error", codeOperation);
            else request.setAttribute("error", codeOperation + " : " + title);  
        
            getServletContext().setAttribute("product", productFacade.findAll());
        }else
        if ("/delete".equals(userPath)){
            String id= request.getParameter("id");

            String codeOperation = productFacade.deleteProduct(Integer.parseInt(id));
            if (codeOperation != "Успешное удаление товара") request.setAttribute("error", codeOperation);
            else request.setAttribute("error", codeOperation);

            getServletContext().setAttribute("product", productFacade.findAll());
        }else
        if ("/add".equals(userPath)){
           String  
                    title = request.getParameter("title"),
                    text = ((request.getParameter("text") != null && !request.getParameter("text").isEmpty()) ?  request.getParameter("text") : "Нет данных");
            Date date = new Date();

            String codeOperation = productFacade.addProduct( title, text, date);
            if (codeOperation != "Успешное добавление товара") request.setAttribute("error", codeOperation);
            else request.setAttribute("error", codeOperation + " : " + title);

            getServletContext().setAttribute("product", productFacade.findAll());
        }
        
        request.getRequestDispatcher("/WEB-INF/views"+userPath+".jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
