/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Articles;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;

/**
 *
 * @author danil
 */
@Stateless
public class ArticlesFacade extends AbstractFacade<Articles> {

    @PersistenceContext(unitName = "MyBlogPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ArticlesFacade() {
        super(Articles.class);
    }
public String addProduct(String title, String text, Date date) 
     {
        try
        {
            if (title != null && !title.isEmpty() && date != null)
            {
               
               newProduct(title, text, date);
                
                return "Успешное добавление товара";
            }
        else {return "Обязательные поля не заполнены";}
        }
        catch (Exception e){
            e.printStackTrace();
            return "Ошибка добавление товара (заполните поля)";
        }
    }
    
     
    public String updateProduct(Integer id,   String title, String text, Date date) 
     {
        try
        {
            if (id != null && title != null && !title.isEmpty() && date != null)
            {
                ProductUpdate(id,  title, text, date);
                //ProductRemove(id);
                return "Успешное обнавление товара";
            }
        else {return "Обязательные поля не заполнены";}
        }
        catch (Exception e){
            e.printStackTrace();
            return "Ошибка добавление товара (заполните обязательные поля)";
        }
    }    
     
 public String deleteProduct(Integer id) 
     {
        try
        {
            if (id != null)
            {
                ProductRemove(id);
                return "Успешное удаление товара";
            }
        else {return "Ошибка удаления";}
        }
        catch (Exception e){
            e.printStackTrace();
            return "Ошибка удаления в ходе запроса";
        }
    } 
    
     
    private void newProduct( String title, String text, Date date) {
        Articles product = new Articles ();
        product.setTitle(title);
        product.setText(text); 
        product.setDate(date);                        
        em.persist(product);
    }
    
    
     private void ProductUpdate(Integer id,  String title, String text,  Date date) {
        Articles product = new Articles(id);
        product.setTitle(title);
        product.setText(text);
        product.setDate(date);                        
        em.merge(product);
    }
     
    private void ProductRemove(Integer id) {
       Articles product = em.find(Articles.class, id);
        if(product != null){
            em.remove(product);
        }
        
    }    
}

